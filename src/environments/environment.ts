// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hmr       : false,
  firebaseConfig : {
    apiKey: "AIzaSyDLQGcse1opDx--lQVFEpfiD4P47twT9oA",
    authDomain: "grifoasto.firebaseapp.com",
    databaseURL: "https://grifoasto.firebaseio.com",
    projectId: "grifoasto",
    storageBucket: "grifoasto.appspot.com",
    messagingSenderId: "398237107059",
    appId: "1:398237107059:web:f8db3e0c0e54c0eecfbb06"
  },
};




/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
