import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { MedirModel } from '../model/corte.model';

@Injectable({
    providedIn: 'root'
})
export class MedidoService {
    entrada: AngularFireList<any>;
    listMedir = new Array<MedirModel>();
    constructor(
        public _firebase: AngularFireDatabase
    ) {
    }

    getMedido(): any {
        this.listMedir = new Array<MedirModel>();       
        this.entrada = this._firebase.list('medido');
        // this.listMedir = [];
        this.entrada.snapshotChanges().forEach(res => {
            res.forEach(e => {
                const maquina = e.payload.toJSON() as MedirModel;
                console.log(maquina);
                const key = e.key;
                this.listMedir.unshift({ key, ...maquina });
            })
        })
        return this.listMedir;

    }
    saveMedidad(newmedido: MedirModel) {
        this.entrada.push({
            fecham: newmedido.fecham,
            horam: newmedido.horam,
            b5m: newmedido.b5m,
            g90m: newmedido.g90m,
            g95m: newmedido.g95m,
        })

    }
    updateMedidad(upmedido: MedirModel) {
        this.entrada.update(upmedido.key, {
            fecham: upmedido.fecham,
            horam: upmedido.horam,
            b5m: upmedido.b5m,
            g90m: upmedido.g90m,
            g95m: upmedido.g95m,
        })
    }
}