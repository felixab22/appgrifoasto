import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { CorteModel } from '../model/corte.model';

@Injectable({
  providedIn: 'root'
})
export class GrifoService {
  surtidor: AngularFireList<any>;
  listCorte = new Array<CorteModel>();  
  constructor(
    public _firebase: AngularFireDatabase
    ) {
    }


  getCorte(corte): any {
    this.listCorte = new Array<CorteModel>();  
    const tipo = localStorage.getItem('tipo'); //puede ser trabajador de dia = Liz y de noche = Nelly
    const enviar = `${tipo}/${corte}` // se agrega el corte de que maquina va a ser
    this.surtidor = this._firebase.list(enviar);
    this.listCorte =[];
    console.log(this.listCorte);
    this.surtidor.snapshotChanges().forEach(res => {
      
      res.forEach(e => {
        const maquina = e.payload.toJSON() as CorteModel;
        const key = e.key;
        this.listCorte.unshift({ key, ...maquina });
      })
    })
    return this.listCorte;

  }
  saveCorte(newcorte: CorteModel) {
    this.surtidor.push({
      persona: newcorte.persona,
      fecha: newcorte.fecha,
      horaentrada: newcorte.horaentrada,
      solesentrada: newcorte.solesentrada,
      galinicio: newcorte.galinicio
    })
    
  }
  updateCorte(newcorte: CorteModel){    
    this.surtidor.update(newcorte.key,{
      persona: newcorte.persona,
      fecha: newcorte.fecha,
      horaentrada: newcorte.horaentrada,
      horasalida: newcorte.horasalida,
      solesentrada: newcorte.solesentrada,
      solessalida: newcorte.solessalida,
      total: newcorte.total,
      galinicio: newcorte.galinicio,
      galfin: newcorte.galfin,
      totalgal: newcorte.totalgal
    })
  } 
}