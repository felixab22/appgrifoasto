import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { CorteModel } from '../model/corte.model';

@Injectable({
  providedIn: 'root'
})
export class AdministrarService {
  surtidor: AngularFireList<any>;
  listCortedia = new Array<CorteModel>();  
  listCortenoche= new Array<CorteModel>();  
  constructor(
    public _firebase: AngularFireDatabase
    ) {
    }


  getCorteDia(corte): any {
    this.listCortedia = new Array<CorteModel>();  
    const enviar = `dia/${corte}` // se agrega el corte de que maquina va a ser
    this.surtidor = this._firebase.list(enviar);
    this.listCortedia =[];
    console.log(this.listCortedia);
    this.surtidor.snapshotChanges().forEach(res => {
      
      res.forEach(e => {
        const maquina = e.payload.toJSON() as CorteModel;
        const key = e.key;
        this.listCortedia.unshift({ key, ...maquina });
      })
    })
    return this.listCortedia;

  }
  getCorteNoche(corte): any {
    this.listCortenoche = new Array<CorteModel>();  
    const enviar = `noche/${corte}` // se agrega el corte de que maquina va a ser
    this.surtidor = this._firebase.list(enviar);
    this.listCortenoche =[];
    console.log(this.listCortenoche);
    this.surtidor.snapshotChanges().forEach(res => {
      
      res.forEach(e => {
        const maquina = e.payload.toJSON() as CorteModel;
        const key = e.key;
        this.listCortenoche.unshift({ key, ...maquina });
      })
    })
    return this.listCortenoche;

  }
}