import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoventaycincoPipe } from './noventaycinco.pipe';
import { PetroleoPipe } from './petroleo.pipe';
import { NoventaPipe } from './noventa.pipe';



@NgModule({
  declarations: [
    NoventaycincoPipe,
    NoventaPipe,
    PetroleoPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NoventaPipe,
    NoventaycincoPipe,
    PetroleoPipe
  ]
})
export class PipesModule { }
