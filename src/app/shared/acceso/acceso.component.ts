import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-acceso',
  templateUrl: './acceso.component.html',
  styleUrls: ['./acceso.component.scss']
})
export class AccesoComponent implements OnInit {
  validatingForm: FormGroup;

  constructor(
    private _router: Router,

  ) { }

  ngOnInit(): void {
    this.validatingForm = new FormGroup({
      nombre: new FormControl('',  [ Validators.required]),
      dni: new FormControl('', [ Validators.required]),
    });
  }
  loginPersonal(form): void {
    if(form.nombre === 'Liz'){      
      this._router.navigate(["/Grifo/Inicio/Diario"]);
      localStorage.setItem('persona','Liz');  
      localStorage.setItem('tipo','dia');  
    } else if( form.nombre === 'Nelly') {
      this._router.navigate(["/Grifo/Inicio/Diario"]);
      localStorage.setItem('persona','Nelly') ; 
      localStorage.setItem('tipo','noche'); 
    } else if( form.nombre === 'Felix') {
      this._router.navigate(["/Grifo/Administrador/Listado"]);
      localStorage.setItem('persona','Liz') ; 
      localStorage.setItem('tipo','dia'); 
    } else {
      console.error('acceso denegado');
      
    }

  }
  get nombre() {
    return this.validatingForm.get('nombre');
  }
  get dni() {
    return this.validatingForm.get('dni');
  }
}
