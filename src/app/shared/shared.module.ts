import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { AccesoComponent } from './acceso/acceso.component';
import { CustonMaterialModule } from '../CustonMaterialModule';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    NavbarComponent, 
    AccesoComponent
  ],
  imports: [
    CommonModule,
    CustonMaterialModule,
    ReactiveFormsModule
  ],
  exports:[
    NavbarComponent, 
    AccesoComponent
  ]
})
export class SharedModule { }
