import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { CustonMaterialModule } from './CustonMaterialModule';
import { PagesModule } from './pages/pages.module';
import { SharedModule } from './shared/shared.module';
import { FormsModule } from '@angular/forms';
// Angular - firebase
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreModule } from '@angular/fire/firestore';

// import { AngularFireStorageModule, BUCKET  } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
// import { NoventaPipe } from './pipes/noventa.pipe';
// import { PipesModule } from './pipes/pipes.module';
// import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module


@NgModule({
  declarations: [
    AppComponent,
    // NoventaPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustonMaterialModule,
    PagesModule,
    SharedModule,
    FormsModule,
    // PipesModule,
    // NgxPaginationModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    // AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)

  ],
  providers: [
    AngularFirestore,
    // { provide: BUCKET, useValue: 'gs://grifoasto.appspot.com' },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill', LOCALE_ID, useValue: "es" } },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
