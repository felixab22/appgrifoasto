export class CorteModel {
    key?:any;
    persona: string;
    fecha?: any;
    horaentrada?: any;
    solesentrada?: number;
    // fechasalida?: any;
    horasalida?: any;
    solessalida?: number;
    total?: number;
    galinicio: number;
    galfin: number;
    totalgal: number;
    // precio?:number;
}
export class PrecioModel {
    key? : any;
    costo?: number;
    tipo: string;
    valor: number;
}
export class MedirModel {
    key? : any;
    horam?: any;
    fecham?: any;
    b5m?: number;
    g90m?: number;
    g95m?: number;
}