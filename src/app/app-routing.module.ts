import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccesoComponent } from './shared/acceso/acceso.component';
import { PagesComponent } from './pages/pages.component';

const routes: Routes = [
  {
    path:'',
    component: AccesoComponent
  },
  {
    path: 'Login',
    component: AccesoComponent
  },
  {
    path: 'Grifo',
    component: PagesComponent,
    children:
    [
      {
        path:'Inicio',
        loadChildren: ()=> import('./pages/corte/corte.module').then(m =>m.CorteModule)
      },
      {
        path:'Administrador',
        loadChildren: ()=> import('./pages/administrar/administrar.module').then(m =>m.AdministrarModule)
      },
      {
        path:'Medir',
        loadChildren: ()=> import('./pages/medicion/medicion.module').then(m =>m.MedicionModule)
      },
      {
        path:'Descarga',
        loadChildren: ()=> import('./pages/descarga/descarga.module').then(m =>m.DescargaModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
