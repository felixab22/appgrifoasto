import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DescargaService } from 'src/app/service/descarga.service';
declare var swal: any;

@Component({
  selector: 'app-anotar',
  templateUrl: './anotar.component.html',
  styleUrls: ['./anotar.component.scss']
})
export class AnotarComponent implements OnInit {
  newDescarga: FormGroup;


  constructor(
    private _formBuilder: FormBuilder,
    private _DescargaSrv: DescargaService,
    private _route : Router

  ) { }

  ngOnInit(): void {

    this.newDescarga = this._formBuilder.group({
      b5m: [, Validators.required],
      g90m: [, Validators.required],
      g95m: [, Validators.required],
    });

  }
  // public hasError = (controlName: string, errorName: string) => {
  //   return this._formBuilder.control[controlName].hasError(errorName);
  // }
  guardarDescarga(desca: any) {
    const hoy = moment();
    const hora = moment().format('LT');
    desca.horam = hora;
    desca.fecham = hoy.format('DD/MM/YYYY');
    console.log(desca);
    this._DescargaSrv.saveDescarga(desca);
    swal("GUARDADO")
      .then(() => {
        this._route.navigate(['/Grifo/Descarga/Enlistar']);
      });
  }
}
