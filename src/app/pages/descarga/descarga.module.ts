import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DescargaRoutingModule } from './descarga-routing.module';
import { EnlistadoComponent } from './enlistado/enlistado.component';
import { AnotarComponent } from './anotar/anotar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CustonMaterialModule } from 'src/app/CustonMaterialModule';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module


@NgModule({
  declarations: [EnlistadoComponent, AnotarComponent],
  imports: [
    CommonModule,
    DescargaRoutingModule,
    ReactiveFormsModule,
    CustonMaterialModule,
    NgxPaginationModule

  ]
})
export class DescargaModule { }
