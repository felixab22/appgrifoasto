import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnlistadoComponent } from './enlistado/enlistado.component';
import { AnotarComponent } from './anotar/anotar.component';

const routes: Routes = [
  {
    path: 'Enlistar',
    component: EnlistadoComponent
  },
  {
    path: 'Registrar',
    component: AnotarComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DescargaRoutingModule { }
