import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MedirModel } from 'src/app/model/corte.model';
import { DescargaService } from '../../../service/descarga.service';
import * as XLSX from 'xlsx'; 

@Component({
  selector: 'app-enlistado',
  templateUrl: './enlistado.component.html',
  styleUrls: ['./enlistado.component.scss']
})
export class EnlistadoComponent implements OnInit {
  listDescarga = new Array<MedirModel>();
  fileName= 'ExcelSheet.xlsx';  
  p: number = 1;

  constructor(
    private _DescargaSrv: DescargaService,
    private _route : Router

  ) { }

  ngOnInit(): void {
    this.listaDescarga();
  }
  listaDescarga() {
    this.listDescarga = this._DescargaSrv.getDescarga();
    console.log(this.listDescarga);
    
  }
  nuevaDescarga(){
    this._route.navigate(['/Grifo/Descarga/Registrar']);
  }
  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-table'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
}
