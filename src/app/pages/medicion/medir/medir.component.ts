import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MedidoService } from '../../../service/medido.service';
declare var swal: any;
@Component({
  selector: 'app-medir',
  templateUrl: './medir.component.html',
  styleUrls: ['./medir.component.scss']
})
export class MedirComponent implements OnInit {
  newedida: FormGroup;
  constructor(
    private _formBuilder: FormBuilder,
    private _medidoSrv: MedidoService,
    private _route : Router

  ) { }

  ngOnInit(): void {
    this.newedida = this._formBuilder.group({
      b5m: [, Validators.required],
      g90m: [, Validators.required],
      g95m: [, Validators.required],
    });
  }
  cancelar() {
    this._route.navigate(['/Grifo/Medir/Mediciones']);
  }
  guardarMedida(medida:any) {
    const hoy = moment();
    const hora = moment().format('LT');
    medida.horam = hora;
    medida.fecham = hoy.format('DD/MM/YYYY');
    console.log(medida);
    this._medidoSrv.saveMedidad(medida);
    swal("GUARDADO")
      .then(() => {
        this._route.navigate(['/Grifo/Medir/Mediciones']);
      });
  }
}
