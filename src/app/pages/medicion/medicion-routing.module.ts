import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { PrecioComponent } from './precio/precio.component';
import { MedicionesComponent } from './mediciones/mediciones.component';
import { MedirComponent } from './medir/medir.component';

const routes: Routes = [
  {
      path:'Mediciones',
      component: MedicionesComponent
  },
  {
    path:'Medida',
    component: MedirComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicionRoutingModule { }
