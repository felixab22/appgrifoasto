import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedicionesComponent } from './mediciones/mediciones.component';
import { MedirComponent } from './medir/medir.component';
import { MedicionRoutingModule } from './medicion-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustonMaterialModule } from 'src/app/CustonMaterialModule';
import { PipesModule } from '../../pipes/pipes.module';
// import { PetroleoPipe } from '../../pipes/petroleo.pipe';



@NgModule({
  declarations: [
    MedicionesComponent, 
    MedirComponent],
  imports: [
    CommonModule,
    MedicionRoutingModule,
    CustonMaterialModule,
    FormsModule,
    HttpClientModule,
    PipesModule,
    ReactiveFormsModule,
    // PetroleoPipe
  ]
})
export class MedicionModule { }
