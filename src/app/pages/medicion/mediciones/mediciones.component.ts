import { Component, OnInit, ViewChild } from '@angular/core';
import { MedirModel } from 'src/app/model/corte.model';
import { MedidoService } from '../../../service/medido.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mediciones',
  templateUrl: './mediciones.component.html',
  styleUrls: ['./mediciones.component.scss']
})
export class MedicionesComponent implements OnInit {
  listMedido = new Array<MedirModel>();
  
  constructor(
    private _medidoSrv: MedidoService,
    private _route : Router

  ) { }

  ngOnInit(): void {
    this.listarCortes();
  }
  listarCortes() {
    // this.listMedido = null;
    // this.listMedido = new Array<MedirModel>();
    this.listMedido = this._medidoSrv.getMedido();
    console.log(this.listMedido);
    
  }
  nuevaMedida(){
    this._route.navigate(['/Grifo/Medir/Medida']);
  }
}
