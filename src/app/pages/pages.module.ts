import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { AppRoutingModule } from '../app-routing.module';
import { SharedModule } from '../shared/shared.module';
// import { BrowserModule } from '@angular/platform-browser';
import { CustonMaterialModule } from './../CustonMaterialModule';



@NgModule({
  declarations: [PagesComponent],
  imports: [
  CommonModule,
    AppRoutingModule,
    SharedModule,
    CustonMaterialModule
    // BrowserModule
  ]
})
export class PagesModule { }
