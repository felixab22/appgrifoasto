import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NuevoCorteComponent } from '../nuevo-corte/nuevo-corte.component';
import { CorteModel } from 'src/app/model/corte.model';
import * as moment from 'moment';

@Component({
  selector: 'app-fin-corte',
  templateUrl: './fin-corte.component.html',
  styleUrls: ['./fin-corte.component.scss']
})
export class FinCorteComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevoCorteComponent>,
    @Inject(MAT_DIALOG_DATA) public nuevoCorte = new CorteModel()
  ) { 
    if(!this.nuevoCorte) {
      this.nuevoCorte = new CorteModel();
    }
  }

  ngOnInit(): void {
    const hora = moment().format('LT');
    this.nuevoCorte.horasalida = hora;
    console.log(this.nuevoCorte);
    
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
