import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiarioComponent } from './diario/diario.component';
// import { PrecioComponent } from './precio/precio.component';

const routes: Routes = [
  {
    path:'Diario',
    component: DiarioComponent
  },
  // {
  //   path:'Precio',
  //   component: PrecioComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorteRoutingModule { }
