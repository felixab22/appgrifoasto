import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CorteModel } from './../../../model/corte.model';
import * as moment from 'moment';

@Component({
  selector: 'app-nuevo-corte',
  templateUrl: './nuevo-corte.component.html',
  styleUrls: ['./nuevo-corte.component.scss']
})
export class NuevoCorteComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<NuevoCorteComponent>,
    @Inject(MAT_DIALOG_DATA) public nuevoCorte = new CorteModel()

  ) { 
    if(!this.nuevoCorte) {
      this.nuevoCorte = new CorteModel();
      this.nuevoCorte.fecha = moment().format('dd/MM/YYYY');
      const personal = localStorage.getItem('persona');
      this.nuevoCorte.persona = personal;      
    }
  }

  ngOnInit(): void { 
    const hoy = moment();
    const hora = moment().format('LT');
    this.nuevoCorte.horaentrada = hora;
    this.nuevoCorte.horasalida = '';
    // this.nuevoCorte.solesentrada
    this.nuevoCorte.fecha = hoy.format('DD/MM/YYYY');
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
