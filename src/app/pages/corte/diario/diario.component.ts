import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { NuevoCorteComponent } from '../nuevo-corte/nuevo-corte.component';
import { GrifoService } from '../../../service/grifo.service';
import { CorteModel } from '../../../model/corte.model';
import { FinCorteComponent } from '../fin-corte/fin-corte.component';

@Component({
  selector: 'app-diario',
  templateUrl: './diario.component.html',
  styleUrls: ['./diario.component.scss']
})
export class DiarioComponent implements OnInit {
  listCorte = new Array<CorteModel>();
  tipocabeza = 'G85';
  cabezara = 'SURTIDOR 1';

  ELEMENT_DATA: any[] = [
    { fecha: '31/07/2020', hora: '08:00pm', persona: 'Nelly', galones: 5479, soles: 32.5, precio: 11.5 }
  ];
  timout;
  displayedColumns: string[] = ['Fecha', 'Hora', 'Persona', 'Galones', 'Soles', 'preciocorte'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  surtidores = [
    { id: 1, titulo: 'SURTIDOR GRANDE CARA 1', tipo: 'PETROLEO', cambio: 'surtidor851', icon: 'opacity', mostrar: true },
    { id: 2, titulo: 'SURTIDOR GRANDE CARA 2', tipo: 'PETROLEO', cambio: 'surtidor852', icon: 'face', mostrar: false },
    { id: 3, titulo: 'SURTIDOR GRANDE CARA 1', tipo: 'GASOHOL 90', cambio: 'surtidor901', icon: 'event', mostrar: false },
    { id: 4, titulo: 'SURTIDOR GRANDE CARA 2', tipo: 'GASOHOL 90 ', cambio: 'surtidor902', icon: 'note', mostrar: false },
    { id: 5, titulo: 'SURTIDOR GRANDE CARA 1', tipo: 'GASOHOL 95', cambio: 'surtidor951', icon: 'loyalty', mostrar: false },
    { id: 6, titulo: 'SURTIDOR GRANDE CARA 2', tipo: 'GASOHOL 95', cambio: 'surtidor952', icon: 'label', mostrar: false },
    { id: 7, titulo: 'SURTIDOR PEQUEÑO', tipo: 'PETROLEO', cambio: 'surtidor853', icon: 'commute', mostrar: false },
    { id: 8, titulo: 'SURTIDOR PEQUEÑO', tipo: 'GASOHOL 90 ', cambio: 'surtidor903', icon: 'dns', mostrar: false },
  ]
  constructor(
    public dialog: MatDialog,
    private _grifoSrv: GrifoService,
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.listarCortes('surtidor851');
  }

  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  enlistar(item) {

    for (let recorrido of this.surtidores) {
      if (recorrido.id === item.id) {
        recorrido.mostrar = true;
      } else {
        recorrido.mostrar = false;
      }
    }
    this.listarCortes(item.cambio);
    this.cabezara = item.titulo;
    this.tipocabeza = item.tipo;
  }
  listarCortes(entrada) {
    this.listCorte = null;
    this.listCorte = new Array<CorteModel>();
    this.listCorte = this._grifoSrv.getCorte(entrada)
  }
  nuevoCorte() { //nuevo corte 
    const dialogRef = this.dialog.open(NuevoCorteComponent, {
      width: '450px',
      height: '450px',
      data: null
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {        
        this._grifoSrv.saveCorte(result);
      } else {
        console.log('salida');
      }
    });
  }

  finalizarCorte(item) { // editar o finalizar el corte
    const dialogRef = this.dialog.open(FinCorteComponent, {
      width: '450px',
      height: '450px',
      data: item
    });
    dialogRef.afterClosed().subscribe((result: CorteModel) => {
      if (result !== undefined) {
        const total = result.solessalida - result.solesentrada;
        const totalgal = result.galfin - result.galfin;
        result.total = total;
        result.totalgal = totalgal;
        this._grifoSrv.updateCorte(result);
        this.listarCortes(item.cambio);
      } else {
        console.log('salida');
      }
    });
  }
}
