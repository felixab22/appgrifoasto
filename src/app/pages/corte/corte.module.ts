import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CorteRoutingModule } from './corte-routing.module';
import { DiarioComponent } from './diario/diario.component';
import { CustonMaterialModule } from './../../CustonMaterialModule';
import { NuevoCorteComponent } from './nuevo-corte/nuevo-corte.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// import { PrecioComponent } from './precio/precio.component';
import { FinCorteComponent } from './fin-corte/fin-corte.component';


@NgModule({
  declarations: [
    DiarioComponent, 
    NuevoCorteComponent,
    //  PrecioComponent, 
     FinCorteComponent
  ],
  imports: [
    CommonModule,
    CorteRoutingModule,
    CustonMaterialModule,
    FormsModule,
    HttpClientModule
  ],
  bootstrap:[NuevoCorteComponent, FinCorteComponent]
})
export class CorteModule { }
