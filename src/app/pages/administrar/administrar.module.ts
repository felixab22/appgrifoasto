import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministraRoutingModule } from './administrar-routing.module';
import { ListadoComponent } from './listado/listado.component';
import { CustonMaterialModule } from 'src/app/CustonMaterialModule';

@NgModule({
  declarations: [ListadoComponent],
  imports: [
    CommonModule,
    AdministraRoutingModule,
    CustonMaterialModule
  ]
})
export class AdministrarModule { }
