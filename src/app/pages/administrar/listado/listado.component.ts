import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CorteModel } from '../../../model/corte.model';
import { AdministrarService } from '../../../service/administrar.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {
  listCortedias = new Array<CorteModel>();
  listCortenoches = new Array<CorteModel>();
  tipocabeza = 'G85';
  cabezara = 'SURTIDOR 1';

  ELEMENT_DATA: any[] = [
    { fecha: '31/07/2020', hora: '08:00pm', persona: 'Nelly', galones: 5479, soles: 32.5, precio: 11.5 }
  ];
  timout;
  displayedColumns: string[] = ['Fecha', 'Hora', 'Persona', 'Galones', 'Soles', 'preciocorte'];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  surtidores = [
    { id: 1, titulo: 'SURTIDOR GRANDE CARA 1', tipo: 'PETROLEO', cambio: 'surtidor851', icon: 'opacity', mostrar: true },
    { id: 2, titulo: 'SURTIDOR GRANDE CARA 2', tipo: 'PETROLEO', cambio: 'surtidor852', icon: 'face', mostrar: false },
    { id: 3, titulo: 'SURTIDOR GRANDE CARA 1', tipo: 'GASOHOL 90', cambio: 'surtidor901', icon: 'event', mostrar: false },
    { id: 4, titulo: 'SURTIDOR GRANDE CARA 2', tipo: 'GASOHOL 90 ', cambio: 'surtidor902', icon: 'note', mostrar: false },
    { id: 5, titulo: 'SURTIDOR GRANDE CARA 1', tipo: 'GASOHOL 95', cambio: 'surtidor951', icon: 'loyalty', mostrar: false },
    { id: 6, titulo: 'SURTIDOR GRANDE CARA 2', tipo: 'GASOHOL 95', cambio: 'surtidor952', icon: 'label', mostrar: false },
    { id: 7, titulo: 'SURTIDOR PEQUEÑO', tipo: 'PETROLEO', cambio: 'surtidor853', icon: 'commute', mostrar: false },
    { id: 8, titulo: 'SURTIDOR PEQUEÑO', tipo: 'GASOHOL 90 ', cambio: 'surtidor903', icon: 'dns', mostrar: false },
  ]
  constructor(
    private _AdministradorSrv : AdministrarService
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.listarCortes('surtidor851');

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  enlistar(item) {

    for (let recorrido of this.surtidores) {
      if (recorrido.id === item.id) {
        recorrido.mostrar = true;
      } else {
        recorrido.mostrar = false;
      }
    }
    this.listarCortes(item.cambio);
    this.cabezara = item.titulo;
    this.tipocabeza = item.tipo;
  }
  listarCortes(entrada) {
    this.listCortedias = null;
    this.listCortenoches = null;
    this.listCortedias = new Array<CorteModel>();
    this.listCortenoches = new Array<CorteModel>();
    this.listCortedias = this._AdministradorSrv.getCorteDia(entrada);
    this.listCortenoches = this._AdministradorSrv.getCorteNoche(entrada);

  }
}
