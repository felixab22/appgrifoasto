FROM node:14.15.0 AS build
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . . 
RUN npm run build --prod


FROM nginx:1.17.1-alpine AS prod-stoge
COPY --from=build /app/dist/AppGrifo /usr/share/nginx/html
EXPOSE 80
CMD ["nginx","-g","daemon off;"]
